function compareSelectedResults() {
    var documents = [];
    $('#results').find('div[name=result]').each(function () {
        console.log($(this).children('input[type=checkbox]').is(':checked'));
        if ($(this).children('input[type=checkbox]').is(':checked')) {
            var link = $(this).children('span').children('a').attr('href');
            documents.push(link.split('?id=')[1]);
        }
    });
    $.ajax({
        type: 'post',
        url: '/comp/compare/',
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({records: documents}),
        success: function (response) {
            var modalTemplate = $(response.data);
            modalTemplate.find('.modal-dialog').css({width: '99%'});
            $('#comparing-results').remove();
            $(document.body).append(modalTemplate);
            $('#comparing-results').modal('show');
        },
        error: function (response) {
            alert('Server error...');
            console.log('Error: ', response);
        }
    });
}


function filterTableRows(obj) {
    var states = obj.value.split(',').filter(Boolean);
    if (states.length > 0) {
        $('.compare-result-item')
            .hide()
            .each(function () {
                var elemStates = $(this).data('state');
                var tmpStates = elemStates.filter(function (elem) {
                    return states.indexOf(elem) !== -1;
                });
                if (tmpStates.length === states.length)
                    $(this).show();
            });
    }
    else {
        $('.compare-result-item[data-state]').show();
    }
}
