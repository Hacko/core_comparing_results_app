import os

SECRET_KEY = 'fake-key'

INSTALLED_APPS = [
    # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',

    # Extra apps
    "password_policies",

    # Local apps
    "tests",
]

FIXTURE_PATH = os.path.join(os.path.dirname(__file__), 'fixtures')
