from core_main_app.components.data import api as data_api
from django.template.loader import get_template
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from core_comparing_results_app.utils.comparing import compare_xml_documents, \
    MESSAGES_MAP


@api_view(('POST',))
@permission_classes((IsAuthenticated,))
def compare_records(request):
    """
    Compare a couple of records from storage.
    We use csv comparing where we can convert XML documents to CSV format
    and compare those paths.
    """
    records_id = request.data.get('records', [])
    template = get_template(
        'core_csv_converter_curate_app/user/'
        'csv-uploading/modals/comparing-results.html'
    )
    if not (isinstance(records_id, list) and len(records_id) in range(2, 5)):
        context = {'message': 'Please, select 2,3 or 4 records.', 'lines': []}
        return Response({'data': template.render(context)})
    records = []
    for object_id in records_id:
        record = data_api.get_by_id(object_id, request.user)
        records.append(record.xml_content)
    lines = compare_xml_documents(*records)
    records_indexes = range(1, len(records) + 1)
    context = {'lines': lines, 'indexes': records_indexes, 'rules': MESSAGES_MAP}
    return Response({'data': template.render(context)})
