from django.conf.urls import url

from core_comparing_results_app.views.user import views as user_views


urlpatterns = [
    url(r'^compare/$', user_views.compare_records, name='core_comparing_results_compare'),
]
