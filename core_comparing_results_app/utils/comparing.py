"""Parser util for curate app
"""
import json

from core_curate_app.utils.csv_parser.xml_converter import XMLConverter

NONE_EXISTS_FIELD_VALUE = '-'

VALUES_EQUAL = 'eq100'
VALUES_DIFFERENT = 'eq0'
HALF_EQUAL = 'eq50'
LESS_THAN_HALF_EQUAL = 'eq50-'
MORE_THAN_HALF_EQUAL = 'eq50+'
EXIST_HALF_PATHS = 'ex50'
PATHS_DIFFERENT = 'ex0'
EXIST_LESS_THAN_HALF_PATHS = 'ex50-'
EXIST_MORE_THAN_HALF_PATHS = 'ex50+'

MESSAGES_MAP = {
    VALUES_EQUAL: 'All values are equal in existed paths',
    VALUES_DIFFERENT: 'All values are different in existed paths',
    HALF_EQUAL: 'Half of values are equal in existed paths',
    LESS_THAN_HALF_EQUAL: 'Less than 50% values are equal in existed paths',
    MORE_THAN_HALF_EQUAL: 'More than 50% values are equal in existed paths',
    EXIST_HALF_PATHS: 'Half of records contains this path',
    PATHS_DIFFERENT: 'Different path in records',
    EXIST_LESS_THAN_HALF_PATHS: 'Less than 50% records contain this path',
    EXIST_MORE_THAN_HALF_PATHS: 'More than 50% records contain this path',
}


def foo(set_values, total_records):
    """
    Analise values for getting state.
    :param set_values: set - Set of values of all records
    :param total_records: int - Number of records
    :return: str
    """
    if len(set_values) == 1:
        return VALUES_EQUAL
    if total_records == len(set_values):
        return VALUES_DIFFERENT
    elif total_records / float(len(set_values)) == total_records / 2.0:
        return HALF_EQUAL
    elif total_records / len(set_values) < 2.0:
        return MORE_THAN_HALF_EQUAL
    return LESS_THAN_HALF_EQUAL


def bar(exists_in, total_records):
    """
    Analise keys for getting state.
    :param exists_in: int - Number of records which contain path.
    :param total_records: int - Number of records
    :return: str
    """
    if total_records == 2 and exists_in != total_records:
        return PATHS_DIFFERENT
    elif exists_in > total_records / 2.0:
        return EXIST_MORE_THAN_HALF_PATHS
    elif exists_in == total_records / 2.0:
        return EXIST_HALF_PATHS
    return EXIST_LESS_THAN_HALF_PATHS


def compare_xml_documents(*xml_documents):
    """
    Compare a couple of xml documents which can contain different values/fields.

    :param xml_documents: list[str] - XML documents as string
    :return: list[dict] - List of dictionaries which contains states and
    help messages for each different.
    """

    # convert string xml values to csv and use dict
    # for comparing which keys are paths from csv
    converter = XMLConverter()
    converter.add_xml_document(*xml_documents)
    converter.process_xml_documents()
    csv_records = []
    common_fields = set()
    total_records = len(xml_documents)
    # walk through all records and get all fields
    for index in range(total_records):
        record = converter.get_ordered_dict(item_index=index)
        csv_records.append(record)
        common_fields |= set(record.keys())
    # walk through all records' fields and calc different values/fields.
    result_lines = []
    for path in common_fields:
        exists_in = 0
        values = list()
        set_values = set()
        for record in csv_records:
            exists = path in record
            exists_in += int(exists)
            if exists:
                set_values.add(record.get(path))
            values.append(record.get(path, NONE_EXISTS_FIELD_VALUE))
        states = []
        # analise keys and values from all records
        if exists_in == total_records:
            states.append(foo(set_values, total_records))
        else:
            states.append(bar(exists_in, total_records))

        expected_states = {EXIST_MORE_THAN_HALF_PATHS, EXIST_HALF_PATHS}
        if expected_states.intersection(states):
            if exists_in > 1:
                states.append(foo(set_values, exists_in))

        messages = '. '.join([MESSAGES_MAP.get(state) for state in states])
        item = {'path': path, 'values': values, 'text': messages,
                'state': json.dumps(states)}
        result_lines.append(item)
    return result_lines
