""" Unit Test CSV Utils
"""
import os
import re

from lxml import etree
from django.conf import settings

from core_comparing_results_app.utils import comparing
from unittest.case import TestCase


class TestCsvRegExpr(TestCase):
    TEST_EXAMPLES_NUMBER = 2

    @classmethod
    def setUpClass(cls):
        cls.test_data_groups = []
        for index in range(1, cls.TEST_EXAMPLES_NUMBER + 1):
            test_file1 = os.path.join(settings.FIXTURE_PATH,
                                      'test{}'.format(index), 'record1.xml')
            test_file2 = os.path.join(settings.FIXTURE_PATH,
                                      'test{}'.format(index), 'record2.xml')
            with open(test_file1, 'r') as f1, open(test_file2, 'r') as f2:
                cls.test_data_groups.append((f1.read(), f2.read()))

    def test_same_records(self):
        """
        Test same records for any different.
        Both records should not contain any different.
        :return:
        """
        xml_content1, xml_content2 = self.test_data_groups[0]
        lines = comparing.compare_xml_documents(xml_content1, xml_content1)
        states = [line['state'] for line in lines]
        self.assertEqual(set(states), {comparing.EQUAL_STATE})

        lines = comparing.compare_xml_documents(xml_content2, xml_content2)
        states = [line['state'] for line in lines]
        self.assertEqual(set(states), {comparing.EQUAL_STATE})

    def test_with_different_keyword_field_states(self):
        """
        Test different for the record/keyword field in the
        first pair of examples.
        """
        xml_content1, xml_content2 = self.test_data_groups[0]
        lines = comparing.compare_xml_documents(xml_content1, xml_content2)
        states = [line['state'] for line in lines]
        expected_set = {comparing.UNEQUAL_STATE, comparing.EQUAL_STATE}
        self.assertSequenceEqual(set(states), expected_set)
        unequal = filter(lambda x: x == comparing.UNEQUAL_STATE, states)
        expected_length = 1
        self.assertEqual(len(unequal), expected_length)

    def test_with_different_keyword_field_path(self):
        """
        Test different for the record/keyword field in the
        first pair of examples.
        """
        xml_content1, xml_content2 = self.test_data_groups[0]
        lines = comparing.compare_xml_documents(xml_content1, xml_content2)
        expected_path = 'record\\keyword'
        expected_values = ['TestKey1', 'TestKey2']
        pair = (expected_path, expected_values)
        results = [
            (line['path'], line['values'])
            for line in lines
            if line['state'] == comparing.UNEQUAL_STATE
        ]
        self.assertSequenceEqual(results, [pair])

    def test_for_none_existing_fields(self):
        """
        Test different for the record/keyword field in the
        first pair of examples.
        """
        xml_content1, xml_content2 = self.test_data_groups[1]
        lines = comparing.compare_xml_documents(xml_content1, xml_content2)
        expected_path = 'record\\material\\Supplier-Data-Sheet\\name'
        results = [
            line['path']
            for line in lines
            if line['state'] in [comparing.DIFFERENT_FIELD_STATE1,
                                 comparing.DIFFERENT_FIELD_STATE2]
        ]
        self.assertSequenceEqual(results, [expected_path])
