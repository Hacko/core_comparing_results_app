========================
Core Comparing Records App
========================

Exploration by example for the curator core project.

Quick start
===========

1. Add "core_comparing_results_app" to your INSTALLED_APPS setting
----------------------------------------------------------------

.. code:: python

    INSTALLED_APPS = [
      ...
      'core_comparing_results_app',
    ]

2. Include the core_comparing_results_app URLconf in your project urls.py
-----------------------------------------------------------------------

.. code:: python

    url(r'^comparing/', include('core_comparing_results_app.urls')),