""" Apps file for setting core package when app is ready
"""
from django.apps import AppConfig


class ComparingResultsAppConfig(AppConfig):
    """ Core application settings.
    """
    name = 'core_comparing_results_app'

